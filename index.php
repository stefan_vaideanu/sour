<?
session_start();
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
      <link rel="stylesheet" href="css/style.css">
      <link href='http://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>
      <script type="text/javascript" src="js/map.js"></script>
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSZLnSErht7XMr7Ib1mg8DFdz3j3voSds&callback=initMaps" async defer></script>
      <script>
         $( function() {
         $( "#datepicker" ).datepicker();
         } );
         function AdressFunction() {
            var checkedBoxes = document.querySelectorAll('input[name=souvenir]:checked');
            alert(checkedBoxes);
            }
         function showResult(str) {
           var e = document.getElementById("country");
           var code = e.options[e.selectedIndex].value;
           if (str.length == 0) {
             //modific fereastra de raspunsuri
             var myNode = document.getElementById("city_input");
             while (myNode.firstChild) {
               myNode.removeChild(myNode.firstChild);
             }
             document.getElementById("city_state").value="";
             return;
           }
           if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
           }else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
           }
           xmlhttp.onreadystatechange = function() {
             if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                //introduc rezultatele
                document.getElementById("city_input").innerHTML = xmlhttp.responseText;
                document.getElementById("city_input").style.border = "1px solid #A5ACB2";
              }
            }
               xmlhttp.open("GET","php/city.php?q="+str+"&c="+code,true);
               xmlhttp.send();
               //document.getElementById("city_state").focus();
            }
      </script>
      <title>Souvenir recommender</title>
   </head>
   <body>
      <header>
         <div class="header-content">
            <p class="current-time">
               <?php include 'php/date.php'; ?>
            </p>
            <h1>
               <span>Souvenir Recommender</span>
            </h1>
            <p>Best suggestions of souvenirs from all over the world and their locations</p>
            <nav>
               <ul class="navigation">
                  <li><a href="#start-app" class="item item-start shadoww" >get started</a></li>
               </ul>
            </nav>
         </div>
         <div class="steps">
            <ul>
               <li class="set-location"><a href="#first-step">1. Set location vizited/to visit</a></li>
               <li class="filter-souvenirs"><a href="#second-step">2. Filter souvenirs</a></li>
               <li class="manage-souvenirs"><a href="#third-step">3. Get souvenirs location</a></li>
            </ul>
         </div>
      </header>
      <hr id="first-step" class="shadoww"/>
      <div id="start-app">
         <div class="filter-location" >
            <h2 class="set-location" >1. Set location vizited/to visit</h2>
            <?php
            session_start();
            if (isset($_POST['get_souvenirs'])){
                
            }
            ?>
            <form method="post" action="#filter-s">
               <label>
                  <span class="city-state">COUNTRY&nbsp;</span>

                  <select name="country" id="country" size="1" onchange="moveToCountry()">
                     <?php include 'php/country.php'; ?>
                  </select>
               </label>
               <label>
                  <span class="city-state">CITY&nbsp;</span>
                  <input id="city_state" autocomplete="off" list="city_input" name="city_state" onkeyup = "showResult(this.value)" placeholder="Select city" oninput="moveToCity()">
                    <datalist id="city_input">
                    </datalist>
               </label>
               <div class="submit-location">
               <button type="submit" value="Get souvenirs" name="get_souvenirs" class="shadoww">Get souvenirs</button>
              </div>
            </form>
         </div>
         <div class="find-location">
            <button onclick="findMe('map-location')" class="locate-me shadoww" id="locate-me-button">Find your location</button>
            <div class="map" id="map-location"></div>
         </div>
      </div>
      <hr id="second-step" class="shadoww"/>
      <div id="filter-s">

         <div class="filter-location" >
            <h2 class="filter-souvenirs" >2. Filter souvenirs</h2>
            <form method="POST" action="#souvenir_list">
               <div class="f_gender">
                  <p>GENDER:</p>
                  <input type="checkbox" name="gender[]" value="Male"> Male
                  <br />
                  <input type="checkbox" name="gender[]" value="Female"> Female
                  <br />
                  <input type="checkbox" name="gender[]" value="Other"> Other
               </div>
               <div class="f_age">
                  <p>AGE:</p>
                  <input type="checkbox" name="age[]" value="Under 18" /> Under 18
                  <br />
                  <input type="checkbox" name="age[]" value="19 to 30" /> 19 to 30
                  <br />
                  <input type="checkbox" name="age[]" value="31 to 50" /> 31 to 50
                  <br />
                  <input type="checkbox" name="age[]" value="Over 50" /> Over 50
               </div>
               <p style="clear:both;"></p>
               <div class="f_relation">
                  <p>RELATION:</p>
                  <input type="checkbox" name="relation[]" value="Friend"> Friend
                  <br />
                  <input type="checkbox" name="relation[]" value="Colleague"> Colleague
                  <br />
                  <input type="checkbox" name="relation[]" value="Partner"> Partner
                  <br />
                  <input type="checkbox" name="relation[]" value="Teacher"> Teacher
                  <br />
                  <input type="checkbox" name="relation[]" value="Mother"> Mother
                  <br />
                  <input type="checkbox" name="relation[]" value="Father"> Father
                  <br />
                  <input type="checkbox" name="relation[]" value="Grand Mother"> Grand Mother
                  <br />
                  <input type="checkbox" name="relation[]" value="Grand Father"> Grand Father
                  <br />
                  <input type="checkbox" name="relation[]" value="Sister"> Sister
                  <br />
                  <input type="checkbox" name="relation[]" value="Brother"> Brother
                  <br />
                  <input type="checkbox" name="relation[]" value="Doughter"> Doughter
                  <br />
                  <input type="checkbox" name="relation[]" value="Son"> Son
                  <br />
                  <input type="checkbox" name="relation[]" value="Pet"> Pet
               </div>
               <div class="f_date">
                  <p class="current-time">
                  </p>
                  <p>CHANGE VISITED DATE:</p>
                  <input type="text" id="datepicker" name="vizited_date" value="<?php include 'php/date.php'; ?>" placeholder="<?php include 'php/date.php'; ?>">
               </div>
               <p style="clear:both;"></p>
               <div class="submit-location apply-filter">
                  <input type="submit" name="submit_filtrs" value="Apply filters" class="shadoww" style="width:160px;">
               </div>
               <div class="submit-location apply-filter">
                  <input type="submit" name="reset_filtrs" value="Reset filters" class="shadoww" style="width:160px;">
               </div>
               <p style="clear:both;"></p>
            </form>
         </div>
         <div class="find-location">
            <div class="souvenirs" id="souvenir_list">
               <form method="POST">
                  <?php include 'php/select.php';?>
               </form>
            </div>
            <div class="submit-location">
               <form action="#map-s">
                  <button id="point_of_interests"  class="shadoww" onclick="AdressFunction()" >points of interests</Button>
               </form>
            </div>
         </div>
      </div>
      <hr id="third-step" class="shadoww"/>
      <div id="map-s">
         <h2 class="manage-souvenirs" >3. Get souvenirs location</h2>
         <div class="map" id="map-souvenirs"></div>
         <div class="submit-location">
            <button onclick="findMe('map-souvenirs')" class="locate-me">Find me on the map</button>
         </div>
         <p><i>Keep some souvenirs of your past, or how will you ever prove it wasn't all a dream</i></p>
      </div>
      <footer></footer>
   </body>
</html>
