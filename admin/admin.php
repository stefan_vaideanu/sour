<!DOCTYPE html>
<html>
   <head>
      <title>Souvenir Recommender</title>
      <link rel="stylesheet" type="text/css" href="admin/css/admin.css">
   </head>
   <body>
      <header>
         <div id="logo">
            <a href="index.php"><span>Souvenir</span>&nbsp;Recommender</a>
                             <?php
        session_start();
        echo ' Admin: '.$_SESSION['username'];
        ?>
         </div>
         <div class="logout_div">
            <label><a href="index.php?action=logout" class="login_field">Logout</a></label>
        </div>
      </header>
      <div class="mobile"></div>
      <div id="container">
         <div class="sidebar">
            <nav>
               <ul>
                  <li><a href="admin.php?page=downloadFile">Download Souvenirs</a></li>
                  <li><a href="admin.php?page=addSouvenir">Add souvenirs</a></li>
                  <li><a href="admin.php?page=removeSouvenir">Edit/Remove souvenirs</a></li>
               </ul>
            </nav>
         </div>
         <div class="content"
            <?php
               if (isset($_GET['page'])){
               $p = $_GET['page'];
               $page = 'admin/sub/'.$p.".php";
               if (file_exists($page)){
                   include($page);
               		}
               }
               ?> 
         </div>
      </div>
   </body>
</html>