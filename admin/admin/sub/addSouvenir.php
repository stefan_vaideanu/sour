
<!DOCTYPE html>
<html>
   <head>
      <title>Add souvenir</title>
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <script>
         $( function() {
         $( "#datepicker_to" ).datepicker();
         $( "#datepicker_from" ).datepicker();
         } );
      </script>
   </head>
   <body>
      <h1>Add Souvenir Form</h1>
      <form method="post">
         <div class="input_value">
            ID:
            <input type="number" name="s_id" value="" class="input_text border_box">
            <br />
            NAME:
            <input type="text" name="s_name" value="" class="input_text border_box">
            <br />
            DESCRIPTION:
            <input type="text" name="s_description" value="" class="input_text border_box">
            <br />
            <script>
               $("input").change(function(e) {
               
                   for (var i = 0; i < e.originalEvent.srcElement.files.length; i++) {
                       
                       var file = e.originalEvent.srcElement.files[i];
                       
                       var img = document.createElement("img");
                       var reader = new FileReader();
                       reader.onloadend = function() {
                            img.src = reader.result;
                       }
                       reader.readAsDataURL(file);
                       $("input").after(img);
                   }
               });
            </script>
            AVAILABLE FROM:
            <input type="date" id="datepicker_from" name="date_from" value="" class="input_text border_box">
            <br />
            AVAILABLE UNTIL:
            <input type="date" id="datepicker_to" name="date_to" value="" class="input_text border_box">
            <br />
            ADRESS:
            <input type="text" name="s_adress" value="" class="input_text border_box">
            <br />
            IMAGE:
            <label for="file-upload" class="custom-file-upload">
            <i class="fa fa-cloud-upload"></i> Upload Image
            </label>
            <input name="s_image" id="file-upload" type="file"/>       
            <br />
         </div>
         <div class="checkbox_value">
            <div class="f_gender">
               <p>GENDER:</p>
               <input type="checkbox" name="gender[]" value="Male"> Male
               <br />
               <input type="checkbox" name="gender[]" value="Female"> Female
               <br />
               <input type="checkbox" name="gender[]" value="Other"> Other
            </div>
            <div class="f_age">
               <p>AGE:</p>
               <input type="checkbox" name="age[]" value="Under 18" /> Under 18
               <br />
               <input type="checkbox" name="age[]" value="19 to 30" /> 19 to 30
               <br />
               <input type="checkbox" name="age[]" value="31 to 50" /> 31 to 50
               <br />
               <input type="checkbox" name="age[]" value="Over 50" /> Over 50
            </div>
         </div>
         <div class="f_relation">
            <p>RELATION:</p>
            <input type="checkbox" name="relation[]" value="Friend"> Friend
            <br />
            <input type="checkbox" name="relation[]" value="Colleague"> Colleague
            <br />
            <input type="checkbox" name="relation[]" value="Partner"> Partner
            <br />
            <input type="checkbox" name="relation[]" value="Teacher"> Teacher
            <br />
            <input type="checkbox" name="relation[]" value="Mother"> Mother
            <br />
            <input type="checkbox" name="relation[]" value="Father"> Father
            <br />
            <input type="checkbox" name="relation[]" value="Grand Mother"> Grand Mother
            <br />
            <input type="checkbox" name="relation[]" value="Grand Father"> Grand Father
            <br />
            <input type="checkbox" name="relation[]" value="Sister"> Sister
            <br />
            <input type="checkbox" name="relation[]" value="Brother"> Brother
            <br />
            <input type="checkbox" name="relation[]" value="Doughter"> Doughter
            <br />
            <input type="checkbox" name="relation[]" value="Son"> Son
            <br />
            <input type="checkbox" name="relation[]" value="Pet"> Pet
         </div>
         <input type="submit" name="add_souvenir" value="ADD SOUVENIR" class="input_text button_c border_box" style="margin-top:20px; height:40px; background-color: #2c3e50;color:white;">
      </form>
   </body>
</html>
<?php
$host = 'localhost';
$dbname = 'sour';
$username = 'root';
$password = '';
try
	{
	$databaseConnection = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
	$databaseConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

catch(PDOEception $e)
	{
	echo $e->getMessage();
	}
    
$sql = $databaseConnection->prepare ('SELECT * FROM souvenir WHERE NAME like ?');


if (isset($_POST['add_souvenir'])){
        $souvenir_values = '';
        $s_id = isset($_POST['s_id']) ? intval($_POST['s_id']) : NULL;
        $s_name = isset($_POST['s_name']) ? $_POST['s_name'] : NULL;
        $s_description = isset($_POST['s_description']) ? $_POST['s_description'] : NULL;
        $s_image = isset($_POST['s_image']) ? $_POST['s_image'] : NULL;
        $gender = isset($_POST['gender']) ? implode(',',$_POST['gender']) : NULL;
        $age = isset($_POST['age']) ? implode(',',$_POST['age']): NULL;
        $relation = isset($_POST['relation']) ? implode(',',$_POST['relation']): NULL;
        
        $date_from = date("Y-m-d", strtotime($_POST['date_from']));
        $date_to = date("Y-m-d", strtotime($_POST['date_to']));
        $date_from = $date_from != '1970-01-01' ? $date_from : NULL;
        $date_to = $date_to != '1970-01-01' ? $date_to : NULL;
        
        $s_adress = isset($_POST['s_adress']) ? $_POST['s_adress']: NULL;
        $sql = $databaseConnection->prepare ('INSERT INTO `souvenir`(`ID`, `NAME`, `DESCRIPTION`, `IMG`, `GENDER`, `AGE`, `RELATION`, `PERIODFROM`, `PERIODTO`, `ADRESS`) VALUES (?,?,?,?,?,?,?,?,?,?)');
        if ($sql->execute ([$s_id, $s_name, $s_description, $s_image, $gender, $age, $relation, $date_from, $date_to, $s_adress])){
                echo 'Souvenir Added';
        }
    }         

?>