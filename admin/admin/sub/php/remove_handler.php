<?php
function get_data(&$row){
    echo '<div class="s_item">';
    echo '<label>';
    echo '<input name="souvenir[]" type="checkbox" value="'.$row['ID'].'" ><img style="height:100px;width:100px;"  src="'.'data:image/jpeg;base64,'.base64_encode( $row['IMG'] ).'" class="s_image shadoww" alt="souvenir"/>';
    echo '<p class="s_text"><span>Name:</span>'.$row['NAME'].'</p>';
    echo '<p class="s_text"><span>Description:</span>'.$row['DESCRIPTION'].'</p>';
    echo '<p class="s_text"><span>Recommended for: </span>';
    echo !empty($row['GENDER'])?$row['GENDER']: 'All genders';
    echo !empty($row['AGE'])?', '.$row['AGE']: ', All ages';
    echo !empty($row['RELATION'])?', '.$row['RELATION']: ', All relations';
    echo '</p>';
    echo '<p class="s_text"><span>Adress:</span>'.$row['ADRESS'].'</p>';
    echo '<p class="s_text"><span>AVAILABLE: </span>';
    echo !empty($row['PERIODFROM'])?'from '.$row['PERIODFROM'].' to '.$row['PERIODTO']: 'All the time';
    echo '</p>';
    echo '</label>';
    echo '</div>';
}

function get_data_input_field(&$row){
    echo '<div>';
    echo '<label>';
    echo '<input name="s_id" type="checkbox" value="'.$row['ID'].'" ><img style="height:100px;width:100px;"  src="'.'data:image/jpeg;base64,'.base64_encode( $row['IMG'] ).'" alt="souvenir"/>';
    echo '<label for="file-upload" class="custom-file-upload">
            <i class="fa fa-cloud-upload"></i>Change Image
            </label>
            <input name="s_image" id="file-upload" type="file"/>       
            <br />';
    echo 'Name:<input name="s_name" type="text" value="'.$row['NAME'].'"/><br />';
    echo 'Description:<input type="text" name="s_description" value="'.$row['DESCRIPTION'].'"/> <br />';
    echo 'Gender: <input name="gender[]" "type="text" value="'.$row['GENDER'].'" /> <br />';
    echo 'Age: <input name="age[]" type="text" value="'.$row['AGE'].'" /> <br />';
    echo 'Relation: <input name="relation[]" type="text" value="'.$row['RELATION'].'" /><br />';
    echo 'Adress:<input type="text" name="s_adress" value="'.$row['ADRESS'].'"/><br />';
        echo 'Available from:
            <input type="date" id="datepicker_from" name="date_from" value="'.$row['PERIODFROM'].'" class="input_text border_box">
            <br />
            Available until:
            <input type="date" id="datepicker_to" name="date_to" value="'.$row['PERIODTO'].'" class="input_text border_box">
            <br />';
    echo '</label>';
    echo '<input type="submit" value="SAVE SOUVENIR" name="save_souvenir" class="border_box"/>';
    echo '</div>';
         
}
$host = 'localhost';
$dbname = 'sour';
$username = 'root';
$password = '';
try
	{
	$databaseConnection = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
	$databaseConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

catch(PDOEception $e)
	{
	echo $e->getMessage();
	}
    
$sql = $databaseConnection->prepare ('SELECT * FROM souvenir WHERE NAME like ?');

if (isset($_POST['search_souvenir'])){
    $search_name = isset($_POST['search_name'])?'%'.$_POST['search_name'].'%':'%';
    if ($sql->execute ([$search_name])){
                 while ($row = $sql->fetch()) {
                    get_data($row);
                    }	
        }
    }
if (isset($_POST['delete_souvenir'])){
        $selected_souvenirs = array();
        $sql = $databaseConnection->prepare ('DELETE
                   FROM souvenir 
                   WHERE ID = ?');
        if (isset($_POST['souvenir'])){
            foreach($_POST['souvenir'] as $souvenir){
                 
                 if ($sql->execute ([intval($souvenir)])){
                        echo intval($souvenir)." Deleted";
                }
            }
            
        }
} 

if (isset($_POST['edit_souvenir'])){
        $selected_souvenirs = array();
        $sql = $databaseConnection->prepare ('SELECT *
                   FROM souvenir 
                   WHERE ID = ?');
        if (isset($_POST['souvenir'])){
            foreach($_POST['souvenir'] as $souvenir){
                 
                 if ($sql->execute ([intval($souvenir)])){
                        while ($row = $sql->fetch()) {
                        get_data_input_field($row);
                    }
                }
            }
            
        }
        if (isset($_POST['save_souvenir'])){ 
            $s_id = isset($_POST['s_id']) ? intval($_POST['s_id']) : NULL;
            $s_name = isset($_POST['s_name']) ? $_POST['s_name'] : NULL;
            $s_description = isset($_POST['s_description']) ? $_POST['s_description'] : NULL;
            $s_image = isset($_POST['s_image']) ? $_POST['s_image'] : NULL;
            $gender = isset($_POST['gender']) ? implode(',',$_POST['gender']) : NULL;
            $age = isset($_POST['age']) ? implode(',',$_POST['age']): NULL;
            $relation = isset($_POST['relation']) ? implode(',',$_POST['relation']): NULL;
            
            $date_from = date("Y-m-d", strtotime($_POST['date_from']));
            $date_to = date("Y-m-d", strtotime($_POST['date_to']));
            $date_from = $date_from != '1970-01-01' ? $date_from : NULL;
            $date_to = $date_to != '1970-01-01' ? $date_to : NULL;
            
            $s_adress = isset($_POST['s_adress']) ? $_POST['s_adress']: NULL;
            $sql = $databaseConnection->prepare ('UPDATE `souvenir` SET `ID`=?,`NAME`=?,`DESCRIPTION`=?,`IMG`=?,`GENDER`=?,`AGE`=?,`RELATION`=?,`PERIODFROM`=?,`PERIODTO`=?,`ADRESS`=? WHERE ID = ?');
            if ($sql->execute ([$s_id, $s_name, $s_description, $s_image, $gender, $age, $relation, $date_from, $date_to, $s_adress,intval($souvenir)])){
                    echo 'Souvenir Added';
            }
    }    
} 
     

?>