var latitude;
var longitude;
var map_l;
var map_s;
var geocoder;

function findMe(map_id){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
    }
    //Get the latitude and the longitude;
    function successFunction(position) {
        var lat = position.coords.latitude;
        var lng = position.coords.longitude;
        codeLatLng(lat, lng)
        var latlong = {lat: lat, lng: lng};

        var map = new google.maps.Map(document.getElementById(map_id), {
          zoom: 12,
          center: latlong
            });
        var marker = new google.maps.Marker({
          position: latlong,
          map: map
            });

        if (map_id == 'map-location'){
            var map = new google.maps.Map(document.getElementById('map-souvenirs'), {
              zoom: 12,
              center: latlong
                });
            var marker = new google.maps.Marker({
              position: latlong,
              map: map
            });
            }
    }

    function errorFunction(){
        alert("Geocoder failed");
    }

    geocoder = new google.maps.Geocoder();

    function codeLatLng(lat, lng) {
        var latlng = new google.maps.LatLng(lat, lng);
        var city_place_id;
        geocoder.geocode({'latLng': latlng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    for (var i=0; i<results[0].address_components.length; i++) {
                        if (results[0].address_components[i].types[0] == "locality") {
                            city= results[0].address_components[i];
                            city_place_id = results[0].place_id
                        }

                        if (results[0].address_components[i].types[0] == "country") {
                            country= results[0].address_components[i];
                        }

                    }

                    select_country = document.getElementById('country_input');
                    select_country.value = country.long_name;
                    select_country.text = country.long_name;
                    select_country.selected = 'selected'

                    select_city = document.getElementById('city_state');
                    select_city.value = city.long_name;
                    select_city.text = city.long_name;
                    select_city.name = city_place_id;

                } else {
                    alert("No results found");
                    }
            } else {
                alert("Geocoder failed due to: " + status);
                }
        });
    }
}

function moveToCountry(countrySele) {
  var e = document.getElementById("country");
  var strUser = e.options[e.selectedIndex].text;
    geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': strUser }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          var map_l = new google.maps.Map(document.getElementById("map-location"), {
            zoom: 6,
            center: results[0].geometry.location
              });

          var map_s = new google.maps.Map(document.getElementById("map-souvenirs"), {
            zoom: 6,
            center: results[0].geometry.location
              });
           var marker_l = new google.maps.Marker({
               map: map_l,
               position: results[0].geometry.location
           });
           var marker_s = new google.maps.Marker({
               map: map_s,
               position: results[0].geometry.location
           });
           map_s.fitBounds(results[0].geometry.viewport);
           map_l.fitBounds(results[0].geometry.viewport);
        } else {
          alert("Geocode was not successful for the following reason: " + status);
        }
    });
}

function moveToCity(countrySele) {
  var e = document.getElementById("city_state");
  var strUser = e.value;
  e = document.getElementById("country");
  strUser = strUser +' , '+ e.options[e.selectedIndex].text;

    geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address': strUser }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          var map_l = new google.maps.Map(document.getElementById("map-location"), {
            zoom: 9,
            center: results[0].geometry.location
              });

          var map_s = new google.maps.Map(document.getElementById("map-souvenirs"), {
            zoom: 9,
            center: results[0].geometry.location
              });
           var marker_l = new google.maps.Marker({
               map: map_l,
               position: results[0].geometry.location
           });
           var marker_s = new google.maps.Marker({
               map: map_s,
               position: results[0].geometry.location
           });
           map_s.fitBounds(results[0].geometry.viewport);
           map_l.fitBounds(results[0].geometry.viewport);
        } else {
          alert("Geocode was not successful for the following reason: " + status);
        }
    });
}

function initMaps() {
    longitude = 0.0;
    latitude = 0.0;
    var latlong = {lat: latitude, lng: longitude};

    var map_l = new google.maps.Map(document.getElementById("map-location"), {
      zoom: 4,
      center: latlong
        });

    var map_s = new google.maps.Map(document.getElementById("map-souvenirs"), {
      zoom: 4,
      center: latlong
        });

    var marker_l = new google.maps.Marker({
      position: latlong,
        });

    var marker_s = new google.maps.Marker({
      position: latlong,
        });

    marker_s.setMap(map_s);
    marker_l.setMap(map_l);
}
