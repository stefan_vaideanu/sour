<?php
function get_data(&$row){
    echo '<div class="s_item">';
    echo '<label>';
    echo '<input name="souvenir[]" type="checkbox" value="'.$row['ID'].'" ><img src="'.'data:image/jpeg;base64,'.base64_encode( $row['IMG'] ).'" class="s_image shadoww" alt="souvenir"/>';
    echo '<p class="s_text"><span>Name:</span>'.$row['NAME'].'</p>';
    echo '<p class="s_text"><span>Description:</span>'.$row['DESCRIPTION'].'</p>';
    echo '<p class="s_text"><span>Recommended for: </span>';
    echo !empty($row['GENDER'])?$row['GENDER']: 'All genders';
    echo !empty($row['AGE'])?', '.$row['AGE']: ', All ages';
    echo !empty($row['RELATION'])?', '.$row['RELATION']: ', All relations';
    echo '</p>';
    echo '<p class="s_text"><span>Adress:</span>'.$row['ADRESS'].'</p>';
    echo '<p class="s_text"><span>AVAILABLE: </span>';
    echo !empty($row['PERIODFROM'])?'from '.$row['PERIODFROM'].' to '.$row['PERIODTO']: 'All the time';
    echo '</p>';
    echo '</label>';
    echo '</div>';
}

?>