<?php
require_once 'dbconfig.php';
include 'get_data.php';

try {
    $pdo = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    
    $select_gender = '';
    $select_relation = '';
    $select_age = '';
    $location = '%';
    $_SESSION['country'] = '%';
    $_SESSION['city_state'] = '%';
    if(isset($_POST['get_souvenirs'])){  
        $country = isset($_POST['country'])?'%'.$_POST['country'].'%':'%';
        $city_state = isset($_POST['city_state'])?'%'.$_POST['city_state'].'%':'%';
        $location = $city_state.$country;
        $sql = $pdo->prepare ('SELECT *
                   FROM souvenir 
                   WHERE ADRESS like ?');
         if ($sql->execute ([$location])){
                 while ($row = $sql->fetch()) {
                    get_data($row);
                    }	
        }
        $_SESSION['country'] = isset($_POST['country'])?'%'.$_POST['country'].'%':'%';
        $_SESSION['city_state'] =isset($_POST['city_state'])?'%'.$_POST['city_state'].'%':'%';
    }
    else if(isset($_POST['submit_filtrs'])){
            if (isset($_POST['gender'])){
                $s_gender = '\''.implode('\',\'',$_POST['gender']).'\'';
                $select_gender = 'AND (GENDER IS NULL or GENDER in ('.$s_gender.'))';
            }
            if (isset($_POST['age'])){
                $s_age = '\''.implode('\',\'',$_POST['age']).'\'';
                $select_age = 'AND (AGE IS NULL or AGE in ('.($s_age).'))';
            }
            if (isset($_POST['relation'])){
                $s_relation = '\''.implode('\',\'',$_POST['relation']).'\'';
                $select_relation = 'AND (RELATION IS NULL or RELATION in ('.($s_relation).'))';
            }
            
            $s_date = date("Y-m-d", strtotime($_POST['vizited_date']));
            $sql = $pdo->prepare ('SELECT *
                       FROM souvenir 
                       WHERE ADRESS like ? AND (((PERIODFROM <= ? AND ? <= PERIODTO) or PERIODFROM IS NULL)'.$select_gender.$select_relation.$select_age.')');
         
            if ($sql->execute ([$_SESSION['city_state'].$_SESSION['country'], $s_date, $s_date])){
                 while ($row = $sql->fetch()) {
                    get_data($row);
                    }	
                }
            }          
    else {
        $sql = $pdo->prepare ('SELECT *
               FROM souvenir
               WHERE ADRESS like ?
               ORDER BY NAME');
 
              if ($sql->execute ([$_SESSION['city_state'].$_SESSION['country']]) ){
            while ($row = $sql->fetch()) {
               get_data($row);
                }    	
            }
        }
    } catch (PDOException $e) {
        die("Could not connect to the database $dbname :" . $e->getMessage());
    }
?>